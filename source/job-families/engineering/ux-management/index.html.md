---
layout: job_page
title: "UX Management"
---

## UX Management Roles at GitLab

Managers in the UX department at GitLab see the team as their product. While they are credible as designers and know the details of what UX Designers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### UX Manager

The User Experience (UX) Manager reports to the Director of UX, and UX
Designers report to the UX Manager.

#### Responsibilities

* Identify improvements for UX design (e.g. look and feel, color, spacing, etc.)
* Hire a world class team of UX Designers
* Help UX Designers grow their skills and experience
* Conducts reviews on Design library and Design system additions
* Hold regular 1:1's with all members of their team
* Create a sense of psychological safety on your team
* Recommend UX technical and process improvements
* Draft quarterly UX OKRs
* Give clear, timely, and actionable feedback
* Improve scheduling process to balance necessary UX improvements
* Works with all parts of the organization (e.g. Backend, Frontend, Build, etc.) to improve overall UX
* Strong sense of ownership, urgency, and drive
* Excellent written and verbal communication skills, especially experience with executive-level communications
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge

#### Requirements

* A minimum of 3 years managing a group of designers
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator)
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills
* You share our [values](/handbook/values), and work in accordance with those values.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter
- Interview with a UX Director (manager)
- Interview with a UX Designer (report)
- Interview with a UX Manager (peer)
- Interview with VP of Engineering

### Director of UX

The User Experience (UX) Director reports to the VP of Engineering, and UX
Managers report to the UX Director.

The Director of UX role extends the [UX Manager](#ux-manager) role.

#### Responsibilities

* Own all of GitLab Product UX
* Set an ambitious UX vision for the department, product, and company
* Manage the UX budget
* Interface regularly with executives on important decisions
* Hire a world class team of managers and UX Designers to work on their teams
* Help their managers and UX Designers grow their skills and experience
* Manage multiple teams and projects
* Hold regular skip-level 1:1's with all members of their team
* Create a sense of psychological safety on your teams
* Drive process improvements
* Drive quarterly UX OKRs
* Give clear, timely, and actionable feedback
* Able to hire great people from their network
* Represent the company publicly at conferences
* Manage across to product management, frontend and other stakeholders to improve processes and ensure delivery
* Establish and promote design guidelines, best practices, and standards
* Work with UX research manager to prioritize quantifiable and actionable feedback through videos and other tools
* Improve scheduling process to balance necessary UX improvements

#### Requirements

* A minimum of 10 years experience managing designers, and leading design for a product company
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator)
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills
* You share our [values](/handbook/values), and work in accordance with those values.

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter
- Interview with UX Manager
- Interview with Engineering Director (peer)
- Interview with VP of Engineering
- Interview with CEO

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
