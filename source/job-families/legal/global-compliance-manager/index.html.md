---
layout: job_page
title: "Global Compliance Manager"
---

The Global Compliance Manager is responsible for assisting in the development, management, and monitoring of GitLab’s global compliance and privacy related obligations and activities. At the direction of the Global Compliance Counsel, the Global Compliance Manager will provide assistance on compliance and privacy matters. This is a remote role, but extensive US, UK, and EU legal compliance experience is a must-have for this role.

## Responsibilities

- Facilitating the organization's compliance with applicable laws, regulations, and best practices by assisting in the development, refinement, and rollout of policies and procedures. Recommend process/policy changes to ensure compliance with all regulations and assist in the documentation or training of those changes.
- Assisting in the development and implementation of internal employee training in cooperation with the People Operations team.
- Evaluating the effectiveness of the training program and developing innovative approaches to training to encourage employee engagement.
- Monitoring legislative and regulatory developments, and providing guidance and training to operational business units relating, but not limited to FCC, DOJ, DOC, and outside US regulatory bodies.
- Assisting business partners in building and maintaining compliance and privacy-related data inventories, data flows, network diagrams information, etc.
- Supporting the day-to-day operations of various global ethics and compliance programs and initiatives, including Code of Conduct, global training, anti-corruption due diligence program, export, gift, hospitality & travel, and employee communication and engagement.
- Assisting with periodic compliance and privacy-related risk assessments and controls evaluations with business partners, to verify whether business units comply with applicable regulations.
- Conducting ongoing reviews of marketing, advertisements, materials, and disclosures.
- Responding to DMCA takedown requests and GDPR requests.

## Requirements for candidate

- Minimum 5-7 years of substantial experience in the areas of legal compliance and data privacy in the US, UK, and EU.
- BA/BS required. Master's, JD, paralegal certificate, or equivalent work experience preferred.
- Thorough knowledge of and experience working with global anti-corruption and anti-slavery laws, privacy regulations (including GDPR), import and export laws, such is required.
- Ability to do in-depth research and to interpret written guidelines and documents.
- Proactive, dynamic and result driven individual with strong attention to detail.
- Ability to understand and communicate complex technical issues.
- Experience working with global teams preferred.
- Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization.
- Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.
- Previous experience in a global startup and remote-first environment would be ideal.
- Experience with open source software a plus.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team). The review process for this role can take a little longer than usual, but if in doubt feel free to check in with the Recruiting team at any point.

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team
* Next, candidates will be invited to schedule a first interview with our Global Compliance Counsel
* Next, candidates will be invited to schedule a second interview with our Senior Director of Legal Affairs
* Next, candidates will be invited to schedule a third interview with our CFO
* Candidates might at this point be invited to schedule with an additional C-Level team member or VP Management member
* Finally, candidates may interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
