---
layout: job_page
title: "Legal Intern"
---

The Legal Intern is a part-time, temporary position, working under the supervision of the Senior Director of Legal Affairs on various legal and compliance projects. This is a remote role.

## Responsibilities

* Assist with legal projects relating to contracts, employment, and legal compliance.
* Assist with other legal needs as requested by the Senior Director of Legal Affairs.

## Requirements

* Minimum of 3 years of work experience.
* Currently enrolled in a JD or paralegal certificate program.
* Ability to do in-depth research and to interpret written guidelines and documents.
* Proactive, dynamic and result driven individual with strong attention to detail.
* Ability to understand and communicate complex issues.
* Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization.
* Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/#screening-call) with a member of our Recruiting team
* Next, candidates will be invited to schedule a 45 minute interview with our Senior Director of Legal Affairs
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring).
