---
layout: job_page
title: "Corporate Events and Brand Manager"
---

Do you love producing amazing customer experiences? Do you thrive on bringing strong brand experience and learning opportunities to life? Detail-oriented marketer with a strong events and engagement marketing background. Leads GitLab’s corporate events direction and vision, aligning to the company’s goals and developing, designing, delivering on a multi-event strategic plan. Also, requires a creative thinker that drives new ideas and innovation into the events program, inspiring the broader team and delivering exceptional event experiences for customers, partners, employees, and other key stakeholders.

## Responsibilities
* Lead strategic and creative development of corporate events and swag strategy to amplify our brand story and image at national and international trade shows and events.
* Proactively manage all event needs, including event identification, selection, and all aspects from branding and content creation to venue selection and event execution.
* Strategically connect business priorities, objectives, and key messages for complete brand experience.
* Define event goals: gather and track ROI, engagement analytics, and feedback to consistently assess and implement opportunities for improvement.
* Develop pre-event, at-event, and post-event marketing plans in conjunction with product marketing, content, demand generation and sales teams. 
* Develop and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Partner with internal cross-functional teams to create event agendas and content that meet engagement objectives.
* Drive project management of events plan and reconcile budgets to execute flawless event plans.
* Develop and manage planning tools (e.g., budget tracking, project timeline, production schedules, event specs, meeting agendas and notes, etc.)
* Lead creation of event communications and promotion for event programs, in partnership with demand generation and sales development teams, to drive attendance.
* Be a team player responsible to build out and manage processes that will ensure the success of our events across functional groups and with production partners.
* Develop and deliver event enablement to include pre-event briefings, at-event briefings, and drive post-event follow-up briefings and event retrospectives.
* Solicit, oversee preparation, and manage keynotes and speaking sessions for all relevant corporate conferences and industry trade shows.
* Create corporate event marketing swag vision and strategy that integrates brand, design, content, corporate events, and PR to increase awareness and engagement, and express brand personality with every interaction.
* Coordinate with design to develop swag strategy as part of brand persona. Evoke spirit of brand and personality in swag items. 

## Requirements
* 5+ years corporate events planning and management experience in the high-tech industry.
* Strategic marketing experience that goes beyond event operations/production, and includes strong understanding of marketing communications, campaigns, event messaging, product and corporate content, and customer experience.
* Independent worker capable of managing multiple deadlines with little supervision.
* Ability to work smart under pressure and efficiently on multiple project requests simultaneously, and to deal with potential for tight deadlines and unexpected complexities.
* Strong communicator and collaborator; able to work with a variety of teams and personalities, possessing excellent verbal and written communications skills.
* Proven skills interacting with executive/senior management teams. 
* Strong project management and decision-making skills.
* Very detail oriented.
* Flexible work schedule and the ability to travel approximately 30% of the time.

## Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Field Marketing Manager
* A 45 minute interview with our Senior Director of Marketing and Sales Development
* A 45 minute interview with our Regional Sales Director - US East Coast
* A 45 minute interview with our Chief Marketing Officer
* Finally, our CEO may choose to conduct a final interview
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
