---
layout: job_page
title: "Marketing Program Manager"
---

You might describe yourself as analytical, creative, organized, and curious. You have experience in planning engaging marketing programs and using modern marketing technology to deliver those programs. You enjoy setting up elegant workflow in marketing automation platforms, conducting quality assurance, measuring results and making informed decisions based on what you learn.

## Responsibilities

* The marketing program manager will report to the Senior Director of Marketing and Sales Development.
* Manage business-to-business marketing programs in Marketo for email campaigns, event promotion, event follow up, drip email nurture series, webinars, and content.
    * Work closely with content and design teams to create landing pages for webinar registration and gated content.
    * Maintain marketing database segmentation to ensure any communications delivered are relevant to the audience receiving them.
    * Work closely with product marketing on mapping marketing database segmentation to personas.
    * Ensure all Marketo programs are linked to salesforce.com campaigns to ensure accurate campaign ROI tracking.
    * Analyze email, webinar, and campaign performance with an eye towards continuous improvement, and sharing insights across the team.
    * Facilitate a marketing program retrospective following completion of each marketing program.
* Create email and landing page split tests to improve the team's understanding of what subject lines, headers, images, copy, and calls-to-action are most effective.
* Work with our marketing and sales teams to become an expert on the company's lead funnel - from web visitor to customer and everything in-between.
* Participate in marketing technology evaluation.
* Document all processes in the handbook and update as needed.

## Requirements

* Excellent spoken and written English.
* Deep empathy for our audience of developers and IT leaders. Our community is changing the world, and you want to help accelerate their success through relevant, useful communications.
* You are obsessed with making customers happy. You know that spam, unclear directions, and unnecessary clicks/forms/inputs can be incredibly annoying.
* Power user of marketing automation software (Marketo preferred).
* Power user of CRM software (Salesforce preferred).
* Experience working in a B2B software marketing team.
* Experience with Open Source software, and/or the developer tools space is preferred.
* Proficiency in MS Excel or Google Sheets.
* Be familiar with or ready to learn how to use GitLab and Git.
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Global Recruiters.
  1. What would differentiate you and make you a great marketing program manager for GitLab?
  1. What is your knowledge of the space that GitLab is in? (e.g. industry trends).
  1. Generally, how would you describe the communication preferences of developers and technical IT management?
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
- Candidates will complete a take-home exercise preparing a spreadsheet of event attendees for use in a marketing program, and send the completed exercise to our Marketing Operations Manager.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Marketing Operations Manager, Online Marketing Manager, and Content Marketing Manager.
- Candidates will then be invited to schedule 45 minute interviews with our Senior Director of Marketing and Sales Development and CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
