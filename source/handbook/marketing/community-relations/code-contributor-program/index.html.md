---
layout: markdown_page
title: "Code Contributor Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Mentors for new contributors
After the first merged MR, make an offer (in an outreach email using the template below) to pair the new contributor with an experienced mentor from the community.  This is for a limited time period only (2 weeks) so that mentors are not overly burdened.  

Currently, the mentors consist primarily of [Core Team members](https://about.gitlab.com/core-team/).  

## Working with the Core Team
There will be a monthly call with members of the [Core Team](https://about.gitlab.com/core-team/) using the Zoom conferencing tool and meetings will also be recorded. Since Core Team members are spread out in different time zones, meeting times will be rotated.   

[Service Desk](https://gitlab.com/gitlab-core-team/general/issues/service_desk) will be used as a communication tool for Core Team members. Anyone can open issues in the Service Desk or send an email to `incoming+gitlab-core-team/general@incoming.gitlab.com`  Core Team members who signed NDAs will also have access to GitLab Slack channels.  

## Communication/collaboration with contributors

### Contributor channel

A GitLab community room is available on [Gitter](https://gitter.im/gitlabhq/community) for people interested in contributing to GitLab.  This is open to everyone to join.

### Hackathons

We will be organizing a quarterly [Hackathon](https://about.gitlab.com/community/hackathon/) for GitLab community members to come together to work on merge requests, participate in tutorial sessions, and support each other on the [GitLab community channel](https://gitter.im/gitlabhq/community).  Agenda, logistics, materials, recordings, and other information for Hackathons will be available on the [Hackathon project pages](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-hackathon).

## Outreach

### Contributor blog post series

Goal is to publish a regular blog post featuring contributors from the community.  The format will be a [casual Q&A with a community member](https://about.gitlab.com/2018/08/08/contributor-post-vitaliy/) and will be posted on the [GitLab blog page](https://about.gitlab.com/blog/).

When developing a blog post, follow the [blog guidelines](/handbook/marketing/blog/#blog-style-guidelines).

### Direct messaging

#### Outreach after the first merged MR

```
Hello NAME,

I’m reaching out to both thank and congratulate you on your first Merge Request (MR) to help improve GitLab.  We appreciate everyone's effort to contribute to the GitLab, especially when it's an individual initiative and we are blessed to have such a wonderful community.  I wanted to offer you a couple of things as you’re getting started with your code contributions to GitLab.

1. Let us know if you'd like to receive the latest GitLab mug with a special hashtag to celebrate your first merged MR.  Please go to [URL] to submit your order (the quantity should be 1), and we will take care of the rest.  When you receive the merchandise, it would be great if you can make a post on twitter with your photo of the mug plus '@gitlab' & '#myfirstMRmerged' in the tweet.
2. Please let me know if you’d like to be paired with one of the experienced GitLab community members (a mentor) for a few weeks to help with your future contributions to GitLab.  

Thanks again for your first MR to GitLab and welcome to the GitLab community!

Sincerely,
YOUR_NAME
```

#### Outreach to inactive contributors

```
Hello NAME,

I’m reaching out to you as we have not seen a merged MR from you since DATE.  The GitLab community would not be the same if it weren’t for contributors like you.  If you haven’t done so lately, you can look for issues that are “Accepting merge requests” as we would welcome your help on these.

Please let me know if you have any questions and I look forward to your continued involvement in the GitLab community.  

Sincerely,
YOUR_NAME
```

## Metrics

Note: this is currently a list to act as a reference of all locations where we can currently gather contributor analytics. It is not *yet* the final set of metrics we will be using to monitor the success of the contributor program with.

"GitLabbers included" column:
- Yes (✓) means the metric includes contributions or contributors that contain both GitLab team members and the wider community
- No (✖) means the metric includes contributions or contributors that contain only the wider community

| METRIC | TIME SERIES | PROJECT | DATA SOURCE | GITLABBERS INCLUDED? | NOTES |
| - | - | - | - | - | - |
| <multiple, TBD> | Release | GitLab EE | [Bitergia dashboard](https://gitlab.biterg.io) | ✖ | - |
| No. of merged contributions | Release | GitLab CE | [Quality dashboard](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit#gid=319422041) | ✖ | To be phased out in favour of the Bitergia Dashboard |
| No. of contributors | All time | GitLab CE | [GitLab contributors app](https://contributors.gitlab.com) | ✓ | - |
| No. of merged contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab organization | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab organization | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution) | ✖ | - |
| No. of WIP open contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&wip=no&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab EE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab EE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution) | ✖ | - |

## Projects

We generally gather data from the GitLab EE project, understood as a superset of GitLab CE. However, there are other projects contributors can submit merge requests to:

- https://gitlab.com/gitlab-org/
- https://gitlab.com/gitlab-org/gitlab-ee
- https://gitlab.com/gitlab-org/gitlab-ce
- https://gitlab.com/gitlab-org/gitlab-runner
- https://gitlab.com/gitlab-org/gitlab-shell
- https://gitlab.com/gitlab-org/omnibus-gitlab
- https://gitlab.com/gitlab-org/gitlab-pages

Additional projects:
- https://gitlab.com/gitlab-org/www-gitlab-com
- https://gitlab.com/gitlab-org/gollum-lib
