---
layout: markdown_page
title: "Marketing Programs"
---


## On this page
{:.no_toc}

- TOC
{:toc}

## Marketing Programs

Marketing Programs focuses on executing, measuring and scaling GitLab's marketing programs such as email campaigns, event promotions, event follow up, drip email nurture series, webinars, and content. Marketing programs also aim to integrate data, personas and content to ensure relevant communications are delivered to the right person at the right time.

## Responsibilities
<table>
  <tr>
    <th>Agnes Oetama</th>
    <th>Jackie Gragnola</th>
    <th>JJ Cordz (temporary until new lead routing rule is determined)</th>
  </tr>
  <tr>
    <td>Webcast Project Management/Set-up/Promotion/Follow-up</td>
    <td>Field Event Support (Exclude List Upload)</td>
    <td>List Upload from Field Events</td>
  </tr>
   <tr>
    <td>Ad-Hoc Emails (For Example - security alert emails, package/pricing changes)</td>
    <td>Drip email campaigns (email nurturing)</td>
    <td></td>
  </tr>
   <tr>
    <td>Bi-weekly newsletter review</td>
    <td>Gating Assets (not including on-demand webcasts)</td>
    <td></td>
  </tr>
</table>

Note 
- Each manager will also own process revamp (including issue template updates and email template design refresh) that falls within their area of resposibility.

Order of assignment for execution tasks: 
- Primary responsible MPM  
- Secondary MPM  (if primary MPM is OOO or not available)
- Marketing OPS (if both MPMs are OOO or not available)


## Requesting Marketing Programs Support for a Field Event

Below is an overview of the process for requesting support. Please contact Jackie Gragnola @jgragnola if you have any questions.

**When to create the event:** When we have a "WIP:" (aka Pending) event. We create the issue then so that it can be on the Marketing Programs team's radar as far in advance as possible.

1. **TEMPLATE:** Create a new issue using the *[Event-Support-mpm template](https://gitlab.com/gitlab-com/marketing/general/blob/master/.gitlab/issue_templates/Event-Support-mpm.md)* WHEN we have a work in progress (pending) event.
2. **NAME:** The name of the event should be *[official name] - [date, year] - MPM Support*
3. **PENDING:** If the event is pending, include *WIP:* in the title until approved.
4. **DETAILS:** Fill in relevant issue at the top of the issue (FMM owner, type, official event name, and date - at a minimum)
5. **META:** Associate the issue to your event meta issue
6. **ASSIGN:** This issue will be automatically assigned to Jackie, who will fill in due dates, and alert the MPM/MOps team.
7. **WHEN APPROVED:** When an event moves from Pending to Approved, remove the WIP from the title and comment in the issue to Jackie to start actioning (i.e. create campaigns, finance tags, etc.).

### Event Channel Types:

*[See full campaign progressions here](https://about.gitlab.com/handbook/business-ops/#conference)*

* **Conference:** Any large event that we have paid to sponsor, have a booth/presence at, and are sending representatives from GitLab. 
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of booth visitors post-event. 
  *  Example: DevOps Enterprise Summit, New York City Technology Forum
* **Field Event:** An event that we have paid to participate in but do not own the registration or event hosting duties.
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of attendees post-event. 
  *  Hint! If we do not own the registration page for the event, but it is not a conference (i.e. a dinner or breakfast), it is likely a Field Event. Comment in the issue to Jackie if you have need help.
  *  Example: Lighthouse Roadshow (hosted by Rancher), All Day DevOps (virtual event hosted by )
* **Owned Event:** This is an event that we have created, own registration and arrange speaker/venue. 
  *  Note: this is considered an Online Channel for bizible reporting because we manage the registration through our website.
  *  Example: GitLab Day Atlanta, Gary Gruver Roadshow

## Requesting an Email  

Process to request an email can be found in the [Business OPS](https://about.gitlab.com/handbook/business-ops/#requesting-an-email) section of the handbook.   

Primary party responsible for various email types can be determined using the [table above](#responsibilities).   
