---
layout: markdown_page
title: "Service Levels and Error Budgets"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Overview

**GitLab.com** has a stated goal of 99.95% uptime, intended to support mission-critical workloads. In  Infrastructure, we are focused on [**observable availability**](/handbook/engineering/infrastructure/blueprint/2018-q4/). In 2018-Q3, we undertook the [first iteration on error budgets](https://about.gitlab.com/handbook/engineering/#error-budgets), and there is work currently underway on issue [`infrastructure/5323`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5323) to define an uptime SLA as an initial attemp to put some structured measurability in place. We are also tasked with performing attribution as part of root cause analysis and carry out the necessary accounting on error budgets.

While [we are arguably improving](https://about.gitlab.com/2018/10/11/gitlab-com-stability-post-gcp-migration/), this improvement is hard to measure and quantify, as we have not properly defined the actual meaning of *uptime*, and we have not yet implemented the framework through which said metric is calculated, tracked and evaluated.

As noted on chapter 3 of the Site Relaibility Engineering book, *Embracing Risk*:

> Site Reliability Engineering seeks to balance the risk of unavailability with the goals of rapid innovation and efficient service operations, so that users’ overall happiness—with features, service, and performance—is optimized.
>
> [...]
>
> Our goal is to explicitly align the risk taken by a given service with the risk the business is willing to bear.

Thus, we embrace risk by strinving to manage it. But we must do so in a stuctured, data-driven fashion. Service levels and error budgets sit at the crossroads of our availibity objectives, our business mandates, and the initiatives we are pursugin to manage risk.

We must first understand which behaviors are important for our users and how we measure and evaluate what the performance of said behaviors. Thus, we will start by defining *service level indicators* and specify *service level objectives* on said metrics. We can then create *service level agreements* to ensure we are committed to our availability objectives.

### SL*X*s

* **Service Level Indicators** (SLIs) define the metrics we use to measure service levels.
* **Service Level Objectives** (SLOs) is a target value for a service level as measured by a SLI.
* **Service Level Agreements** (SLAs) are commitments to our SLOs in a contractual fashion.

##### Time Window

The business has indicated that we must achieve **an availability SLA on GitLab.com of 99.95%**, which is roughly 22 minutes of unplanned downtime per month. We have selected the monthly time frame for this definition for two main reasons:

* It is aligned with the company rythm, which is very much driven by the monthly product release cycles
* It is also aligned with our quarterly OKRs

## Service Levels

In order to define our next iteration on service levels, we are going to pick five SLIs and set the corresponding SLOs per business requirements. Future iterations will break down SLIs and SLOs per service. 

### SLIs

Our initial batch of SLIs will be focused on availabity (success rates) and latency on HTTP and SSH requests against GitLab.com from both an overall point of view and through three specific user paths  (`clone`, `pull` and `push`).

| SLI Type     |                                                              |
| ------------ | ------------------------------------------------------------ |
| Availability | Number of successful HTTP requests over total HTTP requests  |
| Availability | Number of successful SSH requests over total SSH requests    |
| Availability | Number of successful HTTP requests for `clone`, `pull` and `push` operations over total HTTP requests for sid operations |
| Availability | Number of successful SSH requests for `clone`, `pull` and `push` operations over total HTTP requests for sid operations |
| Latency      | Proportion of requests faster than {latency} as a function of payload size for `clone`, `pull` and `push` operations |

### SLOs

The proposed SLO for each of the SLIs is set by the business at 99.95%.

## Error Budgets

Error budgets can be calculated from the defined SLOs. We will move away from the arbitrary severity-based error budget definitions to time-based error budgets. We also need to definte an error budget policy that the entire company adheres to so we can operate at an optimal change speed.

### The Production Queue

We need to track changes and incidents in production (as defined by incident and change management). As we make progress with our service levels and error budgets, most of this will be automated away. But we still need to keep an audit of recorded incidents, which aid during the transition in order to ensure our service level coverage and help us prioritize further developments in this area. We do want to reduce the effort on analuzing this data, and thus the queue must adhere to the *label schema*.

#### Issue Types

The production queue supports four types of issues: `changes`, `incidents`, `deltas` and `hotspots`.

#### Severities

We [standarize on the use of severity levels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels): `S1`, `S2`, `S3 ` and `S4`.

#### Services

We have started to create [structured service definitions](https://gitlab.com/gitlab-com/runbooks/blob/master/services/service-mappings.yml).

Service labels (`service:<service>`) are used to associate services with production issues.

#### Attribution

Incidents must be attributed to specific teams so that error budgets can be properly accounted for.

## Commit

Rather than putting together a grand plan to dogfood CI/CD, let's deploy our [values](/handbook/values/) to help us navigate through this transition:

* **Results: Write Promises Down** Let's commit [OKRs](/company/okrs/) on the development and use of service levels and error budgets. Over time, staying within error budgets becomes an implicit OKR.
* **Results: Tenacity** Let's commit to this transition, as it provides a structured path to track and evaluate GitLab.com's availability.
* **Results: Ambitious** This is a significant change to our development and infrastructure culture, a worthy challenge to pursue as we scale GitLab and GitLab.com.
* **Results: Bias for Action** We have already started to work on this, but we need to make progress.
* **Efficiency: Boring Solutions** Service levels and error budgets are industry-wide best practices.
* **Efficency: Minimum Viable Change** Let's identify and deliver on MVCs.
* **Iteration: Make a Proposal** [Recursive](./).
* **Transparency: Public by Default** [Also recursive](./).

## Futures

In the future we want to explore using a four-week rolling window complemented with weekly summaries and quarterly summarized reports, which will be used to track, calculate and evaluate error budgets in conjunction with incident data.