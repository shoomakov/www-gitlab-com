/* global wNumb */
// Same as .on() but moves the binding to the front of the queue.
$.fn.priorityOn = function(type, selector, data, fn) {
  this.each(function() {
    var $this = $(this);

    var types = type.split(' ');

    for (var i = 0; i < types.length; i++) {
      $this.on(types[i], selector, data, fn);

      var realFn = fn || data;
      var currentBindings = $._data(this, 'events')[types[i]];
      if ($.isArray(currentBindings)) {
        for (var j = 0; j < currentBindings.length; j++) {
          if (currentBindings[j].handler.guid === realFn.guid) {
            currentBindings.unshift(currentBindings.splice(j, 1)[0]);
            break;
          }
        }
      }
    }
  });
  return this;
};

var CategoryRoiCalculator = {
  selector: '.js-roi-calculator',
  $container: [],
  tool_input_has_focus: false,
  dollar_format: wNumb({
    prefix: '$',
    decimals: 0,
    thousand: ','
  }),
  init: function() {
    this.$container = $(this.selector);
    if (this.$container.length > 0) {
      this.setup();
    }
  },
  setup: function() {
    this.$inputs = this.$container.find('.js-roi-calculator-input');
    this.$total = this.$container.find('#js-roi-calculator-total');
    this.$annual_total = this.$container.find('#js-roi-calculator-annual-total');
    this.$inputs.on('change', this.handleChange.bind(this));
    $(document).on('click', '.js-competitor-dropdown li', this.handleDropdownClick.bind(this));
    this.updateTotal();
    this.setDefaultCompetitors();
    this.addOtherToolchainOptions();
  },
  addOtherToolchainOptions: function() {
    var selector = '.js-other-tool-input';
    this.$container.find('.js-competitor-dropdown .dropdown-menu').each(function() {
      $(this).append('<li data-type="other">Other</li>');
    });
    $(document)
      .on('focusin', selector, function() {
        this.tool_input_has_focus = true;
      }.bind(this))
      .on('focusout', selector, function() {
        this.tool_input_has_focus = false;
      }.bind(this))
      .on('keypress', selector, function(event) {
        if (event.which === 13) {
          $(event.currentTarget).blur();
        }
      })
      .on('click', selector, function(event) {
        event.stopPropagation();
      })
      .priorityOn('click', '[data-toggle="dropdown"]', function(event) {
        if (this.tool_input_has_focus) {
          event.stopImmediatePropagation();
        }
      }.bind(this))
    ;
  },
  setDefaultCompetitors: function() {
    var self = this;
    $('.js-competitor-dropdown ul').each(function() {
      var $$ = $(this).children(':first');
      if ( $$.length > 0 ) {
        self.updateDropdownTitle($$);
      }
    });
  },
  handleDropdownClick: function(event) {
    this.updateDropdownTitle( $(event.currentTarget) );
  },
  updateDropdownTitle: function($selected) {
    var $title = $selected.parents('.dropdown').find('.dropdown-title');
    if ( $selected.data('type') === 'other' ) {
      var $input = $title.find('.js-other-tool-input');
      if ( $input.length === 0 ) {
        $input = $('<input type="text" class="js-other-tool-input other-tool-input">');
        $title.html('');
        $title.append($input);
      }
      $input.focus();
    } else {
      $title.html( $selected.html() );
    }
  },
  handleChange: function() {
    this.updateTotal();
  },
  updateTotal: function() {
    var total = this.calculateTotal();
    this.$total.text(this.dollar_format.to(total));
    this.$annual_total.text(this.dollar_format.to(total * 12));
  },
  calculateTotal: function() {
    var total = 0;
    this.$inputs.each(function() {
      var cost = Number.parseFloat($(this).val());
      total += Number.isNaN(cost) ? 0 : cost * 100;
    });
    return total / 100;
  }
};

$(CategoryRoiCalculator.init.bind(CategoryRoiCalculator));
